override CFLAGS += -Wall -Werror

TEST_CFLAGS := -g -pg -fprofile-arcs -ftest-coverage -fsanitize=address

ifneq ($(DEBUG),)
override CFLAGS += $(TEST_CFLAGS)
endif

all: lib twisttest test

.PHONY: lib
lib: libtwist.so

libtwist.so: src/twist.c include/twist/twist.h
	$(CC) -o $@ $(CFLAGS) -Iinclude -fPIC -shared $^

alltest.c: test/test.c $(wildcard test/cutest-*/*)
	./test/cutest-*/make-tests.sh $< > $@

twisttest: PRIVATE_FILES := alltest.c test/test.c src/twist.c $(wildcard test/cutest-*/CuTest.c)
twisttest: alltest.c test/test.c src/twist.c $(wildcard test/cutest-*/CuTest.c)
	$(CC) -o $@ $(PRIVATE_FILES) -I$(wildcard test/cutest-*) -Isrc -Iinclude $(CFLAGS) $(TEST_CFLAGS) -DTEST=1 -fPIE

test: PRIVATE_TEST_TOOL := tools/compute_cov.sh
test: export LD_LIBRARY_PATH=.
test: twisttest tools/compute_cov.sh
	@echo "Running tests..."
	@./$<
	@./$(PRIVATE_TEST_TOOL)

.PHONY: clean
clean:
	@echo "Cleaninig.."
	@-rm -rf libtwist.so twisttest *.o *.gcno *.gcda *.c.gcov gmon.out alltest.c
