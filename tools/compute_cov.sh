#!/usr/bin/env bash

echo 'INFO: Computing coverage....'

out=`gcov twist.c 2>/dev/null`

echo $out

echo 'INFO: Checking for 100% coverage...'
echo $out | grep -q '100.00%'
if [ $? -ne 0 ]; then
	echo 'ERROR: Coverage not 100%, please add unit tests to your change'
	exit 1
fi

echo 'INFO: Coverage is 100%, good job'
exit 0
