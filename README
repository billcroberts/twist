```
_____________      __.___  ____________________
\__    ___/  \    /  \   |/   _____/\__    ___/
  |    |  \   \/\/   /   |\_____  \   |    |   
  |    |   \        /|   |/        \  |    |   
  |____|    \__/\  / |___/_______  /  |____|   
                 \/              \/            
```
noun: a fine strong thread consisting of twisted strands of cotton or silk.

twist is a library for manipulating end prefixed strings that is ABI
compatible with C style strings; thus they are safe to pass to any C
string routine.

However, when used with the twist library routines, string length does not need
to be calculated.

The library provides a simple set of APIs to do common string manipulations, like
concatenation.

The library itself has a unit test suite that is executed with the build, as well
as setup to use gcov to measure testing output.

This library is inspired by "Simple Dynamic String" library aka sds found here:
https://github.com/antirez/sds

This library differs from sds in that it does not store string as length prefixed,
but rather "end prefixed". Thus all twist strings start with a pointer to the end
of the string.

BUILDING:

The simplest: simply drop the twist.c and twist.h files in your project

The harder way:
Type make, you should end up with:
libtwist.so
You can link this as needed

Also, a byproduct of running make is running the unit test suite, aka make test,
automatically. You should see this output (assuming you have gcov installed):

Running tests
....................................................

OK (52 tests)

computing test coverage:
twist.gcno:version '503*', prefer '408*'
twist.gcda:version '503*', prefer version '408*'
File 'src/twist.c'
Lines executed:100.00% 

TESTING:

Our testing is integrated with the build and includes 100% code coverage. The output
is archived in pipelines, which will fail if any test fails.

SUBMITTING SECURITY BUGS:

Please email bill.c.roberts@gmail.com with the details of the vulnerability
as the issue tracker is public. However, do create a generic ticket in the
issue tracker for visibility that will be closed and updated with the details
in the patch that fixes the vulnerability.
