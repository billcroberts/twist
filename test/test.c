#include <stdint.h>
#include <string.h>

#include "CuTest.h"
#include "private.h"
#include "twist/twist.h"

#define LEN(x) (sizeof(x)/sizeof(*x))

void Test_twist_new(CuTest *tc) {

	char *expected = "Hello World";
	twist actual = twist_new(expected);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);
	CuAssert(tc, "Sizes do not match!", strlen(expected) == twist_len(actual));

	twist_free(actual);
}

void Test_twist_new_0_len(CuTest *tc) {

	char *expected = "";
	twist actual = twist_new(expected);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);
	CuAssert(tc, "Sizes do not match!", strlen(expected) == twist_len(actual));

	CuAssertTrue(tc, actual[twist_len(actual)] == '\0');

	twist_free(actual);
}


void Test_twist_new_bad_alloc(CuTest *tc) {

	twist_next_alloc_fails();
	twist actual = twist_new("I should be null");
	CuAssert(tc, "Sizes do not match!", !actual);
}

void Test_twist_new_empty(CuTest *tc) {

	char *expected = "";
	twist actual = twist_new(expected);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);
	CuAssert(tc, "Sizes do not match!", strlen(expected) == twist_len(actual));

	twist_free(actual);
}

void Test_twist_new_null(CuTest *tc) {

	twist actual = twist_new(NULL );
	CuAssertTrue(tc, !actual);
}

void Test_twist_create(CuTest *tc) {

	const char *expected = "onetwothreefourfive";

	const char *data[] = { "one", "two", "three", "four", "five" };

	twist actual = twist_create(data, LEN(data));
	CuAssertPtrNotNullMsg(tc, "twist_create() did not allocate!", actual);
	CuAssertStrEquals(tc, expected, (char * )actual);

	twist_free(actual);
}

void Test_twist_create_null(CuTest *tc) {

	twist actual = twist_create(NULL, 42);
	CuAssertTrue(tc, !actual);
}

void Test_twist_create_embedded_null(CuTest *tc) {

	const char *expected = "onetwothreefourfive";

	const char *data[] = { NULL, "one", NULL, "two", "three", "four", NULL,
			"five", NULL };

	twist actual = twist_create(data, LEN(data));
	CuAssertPtrNotNullMsg(tc, "twist_create() did not allocate!", actual);
	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
}

void Test_twist_dup(CuTest *tc) {

	twist actual = twist_new("Hello World");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", actual);

	twist expected = twist_dup(actual);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", expected);

	CuAssert(tc, "twist_dup() did not result in identical strings!",
			twist_eq(actual, expected));

	twist_free(actual);
	twist_free(expected);
}

void Test_twist_dup_empty(CuTest *tc) {

	twist actual = twist_new("");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", actual);

	twist expected = twist_dup(actual);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", expected);

	CuAssert(tc, "twist_dup() did not result in identical strings!",
			twist_eq(actual, expected));

	twist_free(actual);
	twist_free(expected);
}

void Test_twist_dup_null(CuTest *tc) {

	twist actual = twist_dup(NULL );
	CuAssertTrue(tc, !actual);
}

void Test_twist_end(CuTest *tc) {

	twist x = twist_new("OMG Its a string!");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", x);

	char *end = twist_end(x);
	CuAssertPtrNotNullMsg(tc, "twist_end() did not return valid pointer", x);
	CuAssertPtrEquals(tc, (void * )x + twist_len(x) - 1, (void * )end);
	CuAssertTrue(tc, *end == '!');
	twist_free(x);
}

void Test_twist_end_null(CuTest *tc) {

	char *end = twist_end(NULL );
	CuAssertTrue(tc, !end);
}

void Test_twist_free_null(CuTest *tc) {

	twist_free(NULL );
}

void Test_twist_concat(CuTest *tc) {

	const char *expected = "Original String - Concatenated part";

	twist original = twist_new("Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_concat(original, " - Concatenated part");
	CuAssertPtrNotNullMsg(tc, "twist_concat() failed!", actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(original);
	twist_free(actual);
}

void Test_twist_concat_empty(CuTest *tc) {

	const char *expected = "Original String";

	twist original = twist_new(expected);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_concat(original, "");
	CuAssertPtrNotNullMsg(tc, "twist_concat() failed!", actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(original);
	twist_free(actual);
}

void Test_twistbin_create(CuTest *tc) {

	const char ex[] = { 0xAA, 0x00, 0xBB, 0xCC, 0x00, 0xDD, 0xDD, 0x00, 0xEE,
			0xEE, 0x00, 0xFF };

	const binarybuffer data[] = { { .data = ex, .size = 3 }, { .data = &ex[3],
			.size = 3 }, { .data = &ex[6], .size = 3 }, { .data = &ex[9],
			.size = 3 } };

	twist expected = twistbin_new(ex, LEN(ex));
	CuAssertPtrNotNull(tc, expected);

	twist actual = twistbin_create(data, LEN(data));
	CuAssertPtrNotNull(tc, actual);
	CuAssertTrue(tc, twist_eq(expected, actual));

	twist_free(expected);
	twist_free(actual);
}

void Test_twistbin_new_overflow_1(CuTest *tc) {

	twist actual = twistbin_new((void *) 0xDEADBEEF, ~0);
	CuAssertTrue(tc, !actual);
}

void Test_twistbin_new_overflow_2(CuTest *tc) {

	twist actual = twistbin_new((void *) 0xDEADBEEF, ~0 - sizeof(void *));
	CuAssertTrue(tc, !actual);
}

void Test_twistbin_new_overflow_3(CuTest *tc) {

	twist old = twist_new("hahahahahahahahahahahahaha");
	CuAssertPtrNotNull(tc, old);

	twist actual = twistbin_append(old, (void *) 0xDEADBEEF,
			~0 - sizeof(void *) - 4);
	CuAssertTrue(tc, !actual);

	twist_free(old);
}

void Test_twistbin_new_overflow_4(CuTest *tc) {

	binarybuffer data[] = { { .data = (void *) 0xDEADBEEF, .size = ~0 }, {
			.data = (void *) 0xBADCC0DE, .size = 1 }, };

	twist actual = twistbin_create(data, LEN(data));
	CuAssertTrue(tc, !actual);
}

void Test_twistbin_aappend(CuTest *tc) {

	const char *expected = "No one likes youOMGtests are fun";
	twist old = twist_new("No one likes you");
	CuAssertPtrNotNull(tc, old);

	binarybuffer data[] = { { .data = "OMG", .size = 3 }, { .data =
			"tests are fun", .size = 13 } };

	twist actual = twistbin_aappend(old, data, LEN(data));
	CuAssertPtrNotNull(tc, actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(actual);
}

void Test_twistbin_aappend_null_array(CuTest *tc) {

	const char *expected = "OMGtests are fun";

	binarybuffer data[] = { { .data = "OMG", .size = 3 }, { .data = NULL,
			.size = ~0 }, { .data = "tests are fun", .size = 13 } };

	twist actual = twistbin_aappend(NULL, data, LEN(data));
	CuAssertPtrNotNull(tc, actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(actual);
}

void Test_twistbin_aappend_twist_null(CuTest *tc) {

	twist expected = twist_new("foo");

	twist actual = twistbin_aappend(expected, NULL, 42);
	CuAssertPtrEquals(tc, (void * )actual, (void * )expected);

	actual = twistbin_aappend(expected, (binarybuffer *) 0xDEADBEEF, 0);
	CuAssertPtrEquals(tc, (void * )actual, (void * )expected);

	twist_free(actual);
}

void Test_twist_aappend(CuTest *tc) {

	const char *expected = "No one likes youOMGtests are fun";
	twist old = twist_new("No one likes you");
	CuAssertPtrNotNull(tc, old);

	const char *data[] = { "OMG", NULL, "tests are fun" };

	twist actual = twist_aappend(old, data, LEN(data));
	CuAssertPtrNotNull(tc, actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(actual);
}

void Test_twist_aappend_null_array(CuTest *tc) {

	const char *expected = "OMGtests are fun";

	const char *data[] = { "OMG", "tests are fun" };

	twist actual = twist_aappend(NULL, data, LEN(data));
	CuAssertPtrNotNull(tc, actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(actual);
}

void Test_twist_aappend_twist_null(CuTest *tc) {

	twist expected = twist_new("foo");

	twist actual = twist_aappend(expected, NULL, 42);
	CuAssertPtrEquals(tc, (void * )actual, (void * )expected);

	actual = twist_aappend(expected, (const char **) 0xDEADBEEF, 0);
	CuAssertPtrEquals(tc, (void * )actual, (void * )expected);

	twist_free(actual);
}

void Test_twistbin_create_null(CuTest *tc) {

	twist actual = twistbin_create(NULL, 010101);
	CuAssertTrue(tc, !actual);
}

void Test_twistbin_create_embedded_null(CuTest *tc) {

	const char ex[] = { 0xAA, 0x00, 0xBB, 0xDD, 0x00, 0xEE, 0xEE, 0x00, 0xFF };

	const binarybuffer data[] = { { .data = ex, .size = 3 }, { .data = NULL,
			.size = 0 }, { .data = &ex[3], .size = 3 }, { .data = &ex[6],
			.size = 3 }, };

	twist expected = twistbin_new(ex, LEN(ex));
	CuAssertPtrNotNull(tc, expected);

	twist actual = twistbin_create(data, LEN(data));
	CuAssertPtrNotNull(tc, actual);
	CuAssertTrue(tc, twist_eq(expected, actual));

	twist_free(expected);
	twist_free(actual);
}

void Test_twistbin_concat(CuTest *tc) {

	const char old_str[] = { 0xAA, 0xBB, 0x00, 0xCC };
	const char new_str[] = { 0x11, 0x00, 0x22, 0x33 };
	const char expected[] = { 0xAA, 0xBB, 0x00, 0xCC, 0x11, 0x00, 0x22, 0x33 };

	twist original = twistbin_new(old_str, LEN(old_str));
	CuAssertPtrNotNullMsg(tc, "twistbin_new() did not allocate!", original);

	twist actual = twistbin_concat(original, new_str, LEN(new_str));
	CuAssertPtrNotNullMsg(tc, "twistbin_concat() failed!", actual);

	CuAssertTrue(tc, !memcmp(expected, actual, LEN(expected)));

	twist_free(original);
	twist_free(actual);
}

void Test_twistbin_concat_twist_null(CuTest *tc) {

	const char expected[] = { 0xAA, 0xBB, 0x00, 0xCC };

	twist original = twistbin_new(expected, LEN(expected));
	CuAssertPtrNotNullMsg(tc, "twistbin_new() did not allocate!", original);

	twist actual = twistbin_concat(original, NULL, 0);
	CuAssertPtrNotNullMsg(tc, "twistbin_concat() failed!", actual);

	CuAssertTrue(tc, !memcmp(expected, actual, LEN(expected)));

	twist_free(original);
	twist_free(actual);
}

void Test_twistbin_concat_null_data(CuTest *tc) {

	const char expected[] = { 0xAA, 0xBB, 0x00, 0xCC };

	twist actual = twistbin_concat(NULL, expected, LEN(expected));
	CuAssertPtrNotNullMsg(tc, "twistbin_concat() failed!", actual);

	CuAssertTrue(tc, !memcmp(expected, actual, LEN(expected)));

	twist_free(actual);
}

void Test_twist_concat_twist_null(CuTest *tc) {

	twist expected = twist_new("Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", expected);

	twist actual = twist_concat(expected, NULL );
	CuAssertPtrNotNullMsg(tc, "twist_concat() failed!", actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(actual);
	twist_free(expected);
}

void Test_twist_concat_null_null(CuTest *tc) {

	CuAssertTrue(tc, !twist_concat(NULL, NULL));
}

void Test_twist_concat_empty_null(CuTest *tc) {

	twist expected = twist_new("");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", expected);

	twist actual = twist_concat(expected, NULL );
	CuAssertPtrNotNullMsg(tc, "twist_concat() failed!", actual);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(actual);
	twist_free(expected);
}

void Test_twist_concat_null_empty(CuTest *tc) {

	const char *expected = "";
	twist actual = twist_concat(NULL, expected);
	CuAssertPtrNotNullMsg(tc, "twist_concat() failed!", expected);

	CuAssertStrEquals(tc, expected, actual);
	twist_free(actual);
}

void Test_twist_concat_twist(CuTest *tc) {

	const char *expected = "Original String - Concatenated part";

	twist original = twist_new("Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist part_to_add = twist_new(" - Concatenated part");

	twist actual = twist_concat_twist(original, part_to_add);
	CuAssertPtrNotNullMsg(tc, "twist_concat_twist() failed!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
	twist_free(original);
	twist_free(part_to_add);
}

void Test_twist_concat_twist_null_null(CuTest *tc) {

	CuAssertTrue(tc, !twist_concat_twist(NULL, NULL));
}

void Test_twist_concat_twist_twist_null(CuTest *tc) {

	const char *expected = "Original String";

	twist original = twist_new("Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_concat_twist(original, NULL );
	CuAssertPtrNotNullMsg(tc, "twist_concat_twist() failed!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
	twist_free(original);
}

void Test_twist_concat_twist_null_twist(CuTest *tc) {

	const char *expected = " - Concatenated part";
	twist part_to_add = twist_new(expected);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", part_to_add);

	twist actual = twist_concat_twist(NULL, part_to_add);
	CuAssertPtrNotNullMsg(tc, "twist_concat_twist() failed!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
	twist_free(part_to_add);
}

void Test_twist_eq_equality(CuTest *tc) {

	const char *expected = "im equal";

	twist actual1 = twist_new(expected);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", actual1);

	twist actual2 = twist_new(expected);
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", actual2);

	CuAssertStrEquals(tc, expected, actual1);
	CuAssertStrEquals(tc, expected, actual2);

	CuAssert(tc, "twist_eq() did not return true on identical strings!",
			twist_eq(actual1, actual2));

	twist_free(actual1);
	twist_free(actual2);
}

void Test_twist_eq_equality_null_null(CuTest *tc) {

	CuAssertTrue(tc, twist_eq(NULL, NULL));
}

void Test_twist_eq_equality_twist_null(CuTest *tc) {

	twist x = twist_new("A twist string");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", x);
	CuAssertTrue(tc, !twist_eq(x, NULL));
	twist_free(x);
}

void Test_twist_eq_equality_null_twist(CuTest *tc) {

	twist x = twist_new("A twist string");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", x);
	CuAssertTrue(tc, !twist_eq(NULL, x));
	twist_free(x);
}

void Test_twist_eq_not_equality(CuTest *tc) {

	twist different1 = twist_new("I'm something");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", different1);

	twist different2 = twist_new("I'm something different");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", different2);

	CuAssert(tc, "twist_eq() did not return false on different strings!",
			!twist_eq(different1, different2));

	twist_free(different1);
	twist_free(different2);
}

void Test_twistbin_new_bin_dup_eq(CuTest *tc) {

	uint8_t expected[] = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x00, 0x11, 0x22, 0x33,
			0x44 };

	twist original = twistbin_new(expected, LEN(expected));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	CuAssert(tc, "twist_len() reports different length for binary data!",
			twist_len(original) == LEN(expected));

	twist actual = twist_dup(original);
	CuAssertPtrNotNullMsg(tc, "twist_dup() failed!", actual);

	CuAssert(tc, "memcmp() did not return 0 for same binary strings!",
			!memcmp(expected, actual, LEN(expected)));
	CuAssert(tc, "twist_eq() did not return true for same binary strings!",
			twist_eq(original, actual));

	twist_free(original);
	twist_free(actual);
}

void Test_twistbin_new_null(CuTest *tc) {

	CuAssertTrue(tc, !twistbin_new(NULL, 0));
}

void Test_twistbin_new_not_eq(CuTest *tc) {

	uint8_t data1[] = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x00, 0x11, 0x22, 0x33,
			0x44 };
	uint8_t data2[] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD,
			0xEE };

	twist x = twistbin_new(data1, LEN(data1));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", x);

	twist y = twistbin_new(data2, LEN(data2));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", y);

	CuAssert(tc, "twist_len() reports different length for binary data!",
			twist_len(x) == LEN(data1));
	CuAssert(tc, "twist_len() reports different length for binary data!",
			twist_len(y) == LEN(data2));

	CuAssert(tc, "memcmp() did not return !0 for different binary strings!",
			memcmp(x, y, twist_len(x)));
	CuAssert(tc,
			"twist_eq() did not return false for different binary strings!",
			!twist_eq(x, y));

	twist_free(x);
	twist_free(y);
}

void Test_twistbin_new_not_eq_diff_lengths(CuTest *tc) {

	uint8_t data1[] = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x00, 0x11, 0x22, 0x33,
			0x44 };
	uint8_t data2[] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD,
			0xEE, 0x00, 0xAA, 0xBB };

	twist x = twistbin_new(data1, LEN(data1));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", x);

	twist y = twistbin_new(data2, LEN(data2));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", y);

	CuAssert(tc, "twist_len() reports different length for binary data!",
			twist_len(x) == LEN(data1));
	CuAssert(tc, "twist_len() reports different length for binary data!",
			twist_len(y) == LEN(data2));

	CuAssert(tc, "memcmp() did not return !0 for different binary strings!",
			memcmp(x, y, twist_len(x)));
	CuAssert(tc,
			"twist_eq() did not return false for different binary strings!",
			!twist_eq(x, y));

	twist_free(x);
	twist_free(y);
}

void Test_twist_new_bin_not_eq_diff_lengths(CuTest *tc) {

	uint8_t data1[] = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0x00, 0x11, 0x22, 0x33,
			0x44 };
	uint8_t data2[] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0xAA, 0xBB, 0xCC, 0xDD,
			0xEE, 0x00, 0xAA, 0xBB };

	twist x = twistbin_new(data1, LEN(data1));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", x);

	twist y = twistbin_new(data2, LEN(data2));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", y);

	CuAssert(tc, "twist_len() reports different length for binary data!",
			twist_len(x) == LEN(data1));
	CuAssert(tc, "twist_len() reports different length for binary data!",
			twist_len(y) == LEN(data2));

	CuAssert(tc, "memcmp() did not return !0 for different binary strings!",
			memcmp(x, y, twist_len(x)));
	CuAssert(tc,
			"twist_eq() did not return false for different binary strings!",
			!twist_eq(x, y));

	twist_free(x);
	twist_free(y);
}

void Test_twist_append(CuTest *tc) {

	const char *expected = "My Original String - cool";

	twist original = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_append(original, " - cool");
	CuAssertPtrNotNullMsg(tc, "twist_append() did not re-allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
}

void Test_twist_vappend(CuTest *tc) {

	const char *expected = "My Original String - cool";

	twist original = twist_new("My");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_vappend(original, 3, " Original", " String",
			" - cool");
	CuAssertPtrNotNullMsg(tc, "twist_append() did not re-allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
}

void Test_twist_vappend_null(CuTest *tc) {

	const char *expected = "My Original String";

	twist original = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_vappend(original, 2, NULL, NULL );
	CuAssertPtrNotNullMsg(tc, "twist_append() did not re-allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
}

void Test_twist_vappend_null_null(CuTest *tc) {

	twist actual = twist_vappend(NULL, 0);
	CuAssertTrue(tc, !actual);
}

void Test_twist_vappend_embedded_null(CuTest *tc) {

	const char *expected = "My Original String - cool";

	twist original = twist_new("My");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_vappend(original, 7, NULL, " Original", NULL,
			" String", NULL, " - cool", NULL );
	CuAssertPtrNotNullMsg(tc, "twist_append() did not re-allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
}

void Test_twist_append_bad_alloc(CuTest *tc) {

	twist original = twist_new("I'm Good");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist_next_alloc_fails();
	twist actual = twist_append(original, "I fail");
	CuAssertTrue(tc, !actual);

	twist_free(original);
}

void Test_twistbin_append(CuTest *tc) {

	const char first[] = { 0x01, 0x55, 'A', 'B' };
	const char second[] = { 0x00, 'x', 0x00 };
	const char expected[] = { 0x01, 0x55, 'A', 'B', 0x00, 'x', 0x00 };

	twist tfirst = twistbin_new(first, LEN(first));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", tfirst);

	twist actual = twistbin_append(tfirst, second, LEN(second));
	CuAssertPtrNotNull(tc, actual);

	CuAssertTrue(tc, !memcmp(expected, actual, LEN(expected)));
	twist_free(actual);
}

void Test_twistbin_vappend(CuTest *tc) {

	const char first[] = { 0x01, 0x55, 'A', 'B' };
	const char second[] = { 0x00, 'x', 0x00 };
	const char third[] = { 0x66, 'b', 0x34 };

	const char expected[] = { 0x01, 0x55, 'A', 'B', 0x00, 'x', 0x00, 0x66, 'b',
			0x34 };

	twist tfirst = twistbin_new(first, LEN(first));
	CuAssertPtrNotNull(tc, expected);

	twist actual = twistbin_vappend(tfirst, 4, second, LEN(second), third,
			LEN(third));
	CuAssertPtrNotNull(tc, actual);

	CuAssertTrue(tc, !memcmp(expected, actual, LEN(expected)));
	twist_free(actual);
}

void Test_twistbin_vappend_null_0(CuTest *tc) {

	twist actual = twistbin_vappend(NULL, 0);
	CuAssertTrue(tc, !actual);
}

void Test_twistbin_vappend_null_not_even_arg_count(CuTest *tc) {

	twist actual = twistbin_vappend(NULL, 41);
	CuAssertTrue(tc, !actual);
}

void Test_twistbin_bad_args(CuTest *tc) {

	twist actual = twistbin_vappend(NULL, 4, "stuff", 4, NULL, 42);
	CuAssertTrue(tc, !actual);
}

void Test_twistbin_embedded_empty_args(CuTest *tc) {

	twist actual = twistbin_vappend(NULL, 6, "stuff", 5, NULL, 0, "thing", 5);
	CuAssertPtrNotNull(tc, actual);

	CuAssertStrEquals(tc, "stuffthing", actual);
	twist_free(actual);
}

void Test_twist_append_twist(CuTest *tc) {

	const char first[] = { 0x01, 0x55, 'A', 'B' };
	const char second[] = { 0x00, 'x', 0x00 };
	const char expected[] = { 0x01, 0x55, 'A', 'B', 0x00, 'x', 0x00 };

	twist tfirst = twistbin_new(first, LEN(first));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", tfirst);

	twist tsecond = twistbin_new(second, LEN(second));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", tsecond);

	twist actual = twist_append_twist(tfirst, tsecond);
	CuAssertPtrNotNullMsg(tc, "twistbin_append() failed!", actual);

	CuAssertTrue(tc, !memcmp(expected, actual, LEN(expected)));
	twist_free(actual);
	twist_free(tsecond);
}

void Test_twist_append_twist_null(CuTest *tc) {

	twist expected = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", expected);

	twist actual = twist_append(expected, NULL );
	CuAssertPtrNotNullMsg(tc, "twist_append() did not re-allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);
	CuAssertTrue(tc, twist_eq(actual, expected));

	twist_free(actual);
}

void Test_twistbin_append_twist_null(CuTest *tc) {

	const char first[] = { 0x01, 0x55, 'A', 'B' };

	twist tfirst = twistbin_new(first, LEN(first));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", tfirst);

	twist actual = twistbin_append(tfirst, NULL, 0);
	CuAssertPtrNotNullMsg(tc, "twistbin_append() failed!", actual);

	CuAssertPtrEquals(tc, (void * )tfirst, (void * )actual);
	CuAssertTrue(tc, !memcmp(first, actual, LEN(first)));

	twist_free(actual);
}

void Test_twist_append_twist_twist_null(CuTest *tc) {

	const char first[] = { 0x01, 0x55, 'A', 'B' };

	twist tfirst = twistbin_new(first, LEN(first));
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", tfirst);

	twist actual = twist_append_twist(tfirst, NULL );
	CuAssertPtrNotNullMsg(tc, "twist_append_twist() failed!", actual);

	CuAssertPtrEquals(tc, (void * )tfirst, (void * )actual);
	CuAssertTrue(tc, !memcmp(first, actual, LEN(first)));

	twist_free(actual);
}

void Test_twist_append_null_string(CuTest *tc) {

	const char *expected = "this is some string";

	twist actual = twist_append(NULL, expected);
	CuAssertPtrNotNullMsg(tc, "twist_append() did not re-allocate!", actual);

	CuAssertStrEquals(tc, expected, actual);

	twist_free(actual);
}

void Test_twistbin_append_null_string(CuTest *tc) {

	const char second[] = { 0x01, 0x00, 'A', 'B' };

	twist actual = twistbin_append(NULL, second, LEN(second));
	CuAssertPtrNotNullMsg(tc, "twistbin_append() failed!", actual);

	CuAssertTrue(tc, !memcmp(second, actual, LEN(second)));

	twist_free(actual);
}

void Test_twist_append_twist_null_string(CuTest *tc) {

	const char second[] = { 0x01, 0x00, 'A', 'B' };

	twist tsecond = twistbin_new(second, LEN(second));
	CuAssertPtrNotNullMsg(tc, "twistbin_new() failed!", tsecond);

	twist actual = twist_append_twist(NULL, tsecond);
	CuAssertPtrNotNullMsg(tc, "twist_append_twist() failed!", actual);

	CuAssertTrue(tc, !memcmp(second, actual, LEN(second)));

	twist_free(actual);
}

void Test_twist_append_null_null(CuTest *tc) {

	CuAssertTrue(tc, !twist_append(NULL, NULL));
}

void Test_twistbin_append_null_null(CuTest *tc) {

	CuAssertTrue(tc, !twistbin_append(NULL, NULL, 0));
}

void Test_twist_append_twist_null_null(CuTest *tc) {

	CuAssertTrue(tc, !twist_append_twist(NULL, NULL));
}

void Test_twist_truncate_smaller(CuTest *tc) {

	twist original = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	size_t orig_len = twist_len(original);

	twist actual = twist_truncate(original, orig_len - 4);
	CuAssertPtrNotNullMsg(tc, "twist_truncate() did not allocate!", actual);
	CuAssertTrue(tc, orig_len - 4 == twist_len(actual));

	twist_free(actual);
}

void Test_twist_truncate_bigger(CuTest *tc) {

	twist original = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	size_t orig_len = twist_len(original);

	twist actual = twist_truncate(original, orig_len + 104);
	CuAssertPtrNotNullMsg(tc, "twist_truncate() did not allocate!", actual);
	CuAssertTrue(tc, orig_len + 104 == twist_len(actual));

	twist_free(actual);
}

void Test_twist_truncate_bigger_bad_alloc(CuTest *tc) {

	twist original = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	size_t orig_len = twist_len(original);
	twist_next_alloc_fails();
	twist actual = twist_truncate(original, orig_len + 104);
	CuAssertTrue(tc, !actual);
	twist_free(original);
}

void Test_twist_truncate_same(CuTest *tc) {

	twist original = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	size_t orig_len = twist_len(original);

	twist actual = twist_truncate(original, orig_len);
	CuAssertPtrNotNullMsg(tc, "twist_truncate() did not allocate!", actual);
	CuAssertTrue(tc, orig_len == twist_len(actual));
	CuAssertPtrEquals(tc, (void * )original, (void * )actual);

	twist_free(actual);
}

void Test_twist_truncate_zero(CuTest *tc) {

	twist original = twist_new("My Original String");
	CuAssertPtrNotNullMsg(tc, "twist_new() did not allocate!", original);

	twist actual = twist_truncate(original, 0);
	CuAssertPtrNotNullMsg(tc, "twist_truncate() did not allocate!", actual);
	CuAssertTrue(tc, 0 == twist_len(actual));

	twist_free(actual);
}

void Test_twist_truncate_null(CuTest *tc) {

	CuAssertTrue(tc, !twist_truncate(NULL, 0));
}

void Test_twist_unhexlify_null(CuTest *tc) {

	twist null = twistbin_unhexlify(NULL);
	CuAssertTrue(tc, null == NULL);
}

void Test_twist_unhexlify_0_len(CuTest *tc) {

	twist zero_len = twistbin_unhexlify("");
	CuAssertTrue(tc, twist_len(zero_len) == 0);
	CuAssertTrue(tc, zero_len[0] == '\0');
	twist_free(zero_len);
}

void Test_twist_unhexlify_odd_len(CuTest *tc) {

	twist odd = twistbin_unhexlify("a");
	CuAssertTrue(tc, odd == NULL);

	odd = twistbin_unhexlify("abc");
	CuAssertTrue(tc, odd == NULL);


	odd = twistbin_unhexlify("defab");
	CuAssertTrue(tc, odd == NULL);
}

void Test_twist_unhexlify_even_len(CuTest *tc) {

	/* Test all the characters */
	char raw[] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0xab, 0xcd, 0xef };
	twist even = twistbin_unhexlify("0123456789ABCDEFabcdef");
	CuAssertPtrNotNullMsg(tc, "twistbin_unhexlify() failed", even);
	CuAssertTrue(tc, even[twist_len(even)] == '\0');
	CuAssertTrue(tc, sizeof(raw) == twist_len(even));
	CuAssertTrue(tc, !memcmp(even, raw, sizeof(raw)));
	twist_free(even);

	/* test smallest valid */
	even = twistbin_unhexlify("01");
	CuAssertPtrNotNullMsg(tc, "twistbin_unhexlify() failed", even);
	CuAssertTrue(tc, even[twist_len(even)] == '\0');
	twist_free(even);

	even = twistbin_unhexlify("01a2b3c4");
	CuAssertPtrNotNullMsg(tc, "twistbin_unhexlify() failed", even);
	CuAssertTrue(tc, even[twist_len(even)] == '\0');
	twist_free(even);
}

void Test_twist_unhexlify_bad_chars(CuTest *tc) {

	/* Test all bad characters */
	twist bad = twistbin_unhexlify("-(*&");
	CuAssertTrue(tc, bad == NULL);

	/* Test start good end bad */
	bad = twistbin_unhexlify("abcdef0123-(*&");
	CuAssertTrue(tc, bad == NULL);

	/* Test start bad end good*/
	bad = twistbin_unhexlify("-+=~`abcdef");
	CuAssertTrue(tc, bad == NULL);

	/* test upper nibble good lower bad */
	bad = twistbin_unhexlify("a-");
	CuAssertTrue(tc, bad == NULL);
}

void Test_twist_unhexlify_failed_alloc(CuTest *tc) {

	/* Test all bad characters */
	twist_next_alloc_fails();
	twist bad = twistbin_unhexlify("abcd");
	CuAssertTrue(tc, bad == NULL);
}

void Test_twist_hexlify_null(CuTest *tc) {

	twist null = twist_hexlify(NULL);
	CuAssertTrue(tc, null == NULL);
}

void Test_twist_hexlify_0_len(CuTest *tc) {

	twist zero_len = twist_new("");
	twist hex_zero_len = twist_hexlify(zero_len);

	CuAssertTrue(tc, twist_len(hex_zero_len) == 0);
	CuAssertTrue(tc, hex_zero_len[0] == '\0');

	twist_free(hex_zero_len);
	twist_free(zero_len);
}

void Test_twist_hexlify_good(CuTest *tc) {

	char raw[] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };
	twist raw_data = twistbin_new(raw, sizeof(raw));
	twist hex_data = twist_hexlify(raw_data);

	CuAssertTrue(tc, twist_len(hex_data) == sizeof(raw) * 2);
	CuAssertTrue(tc, hex_data[twist_len(hex_data)] == '\0');
	CuAssertTrue(tc, !strcasecmp("0123456789ABCDEF", hex_data));

	twist_free(hex_data);
	twist_free(raw_data);
}

void Test_twist_hexlify_alloc_fail(CuTest *tc) {

	char raw[] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };
	twist raw_data = twistbin_new(raw, sizeof(raw));
	twist_next_alloc_fails();
	twist hex_data = twist_hexlify(raw_data);
	CuAssertTrue(tc, hex_data == NULL);

	twist_free(raw_data);
}

void Test_twist_hexlify_unhelify(CuTest *tc) {

	char *raw_hex = "0123456789ABCDEF";
	char raw_data[] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF };

	twist raw = twistbin_new(raw_data, sizeof(raw_data));
	twist hex = twist_hexlify(raw);

	CuAssertTrue(tc, sizeof(raw_data) == twist_len(raw));
	CuAssertTrue(tc, strlen(raw_hex) == twist_len(hex));

	CuAssertTrue(tc, !strcasecmp(raw_hex, hex));
	CuAssertTrue(tc, !memcmp(raw_data, raw, sizeof(raw_data)));

	twist_free(raw);
	twist_free(hex);
}
